#[macro_use]
extern crate tower_web;

use std::net::SocketAddr;
// use tokio::prelude::*;
use tower_web::ServiceBuilder;

#[derive(Clone, Debug)]
pub struct HelloWorld {
    motd: String,
}

impl_web! {
    impl HelloWorld {
        #[get("/")]
        fn hello_world(&self) -> Result<String, ()> {
            Ok(format!("Hello there! Message of the Day is, {}", self.motd))
        }

        #[post("/")]
        fn print_stdout(&self) -> Result<&'static str, ()> {
            println!("Hello from the Web");
            Ok("Println done!")
        }
    }
}

pub fn main() {
    let addr: SocketAddr = "127.0.0.1:8080".parse().expect("Invalid address");
    println!("Listening on http://{}", addr);

    ServiceBuilder::new()
        .resource(HelloWorld {
            motd: "Just do it".to_string(),
        })
        .run(&addr)
        .unwrap();
}
